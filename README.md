## 平台简介

**这是基于ruoyi-vue集成自研工作流Warm-Flow, 后续会调整代码生成，可以生成工作流业务代码**

**希望一键三连[warm-flow](https://gitee.com/warm_4/warm-flow.git)，你的⭐️ Star ⭐️是我持续开发的动力**


## 个人项目
一、 **如需ruoyi的升级版** ，集成mybatis-plus,多租户，satoken。已作为自己公司开发脚手架，请切分支hh-vue

> [hh-vue](https://gitee.com/min290/hh-vue/tree/hh-vue/)


二、 **基于ruoyi-vue原版完整集成mybatis-flex** ，做到最小改动，会同步更新ruoyi，可放心食用

    **代码生成已经提供flex版本** ，需要导入test.sql

> [springboot集成方式](https://gitee.com/min290/RuoYi-Vue.git)
>
> [spring集成方式](https://gitee.com/min290/RuoYi-Vue/tree/ruoyi-spring-flex/)



三、 **重磅推出solon版本（去spring版本）** ，也已经集成mybatis-flex/mybatis-plus
参照ruoyi改造，jdk17+satoken+redisx/redisson+mybaits-flex+hutool+jackson+mapstruct+poi

> [warm](https://gitee.com/min290/warm)

**四、vue3版本已经上线**

https://gitee.com/min290/warm-vue3.git


## 工作流

### warm-flow

此项目是极其简单的工作流，没有太多设计，代码量少，并且只有6张表，个把小时就可以看完整个设计。使用起来方便

1. 支持简单的流程流转，比如跳转、回退、审批
2. 支持角色、部门和用户等权限配置
3. 官方提供简单流程封装demo项目，很实用
4. 支持多租户
5. 支持代办任务和已办任务，通过权限标识过滤数据
6. 支持互斥网关，并行网关（会签、或签）
7. 可跳转任意节点
8. 支持条件表达式，可扩展
9. 同时支持spring和solon
10. 兼容java8和java17,理论11也可以
11. 支持不同orm框架和数据库扩展

>   **联系方式：qq群：778470567， 微信：warm-houhou**
>   **工作流地址**：https://gitee.com/warm_4/warm-flow.git

### 演示地址

- admin/admin123

演示地址：http://www.hhzai.top:81

### 演示图

<table>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1697704379975758657/558474f6_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703576997421577844/a1dc2737_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1703577051212751284/203a05b0_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703577120823449150/ba952a84_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1703577416508497463/863d8da1_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703641952765512992/dc187080_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1703639870569018221/453a0e0e_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703639949778635820/34a6c14e_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1703640045465410604/c14affda_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703641581976369452/e4629da5_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1703640080823852176/bdf9a360_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703640099939146504/b19b2b85_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1703641659022331552/cc4e0af2_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703641675840058630/3430da37_2218307.png"/></td>
    </tr>
    <tr>
        <td><img src="https://foruda.gitee.com/images/1703641687716655707/62a8b20c_2218307.png"/></td>
        <td><img src="https://foruda.gitee.com/images/1703641702939748288/6da6c4f6_2218307.png"/></td>
    </tr>
</table>