import request from '@/utils/request'

// 查询OA 请假申请列表
export function listLeave(query) {
  return request({
    url: '/system/leave/list',
    method: 'get',
    params: query
  })
}

// 查询OA 请假申请详细
export function getLeave(id) {
  return request({
    url: '/system/leave/' + id,
    method: 'get'
  })
}

// 新增OA 请假申请
export function addLeave(data) {
  return request({
    url: '/system/leave',
    method: 'post',
    data: data
  })
}

// 修改OA 请假申请
export function updateLeave(data) {
  return request({
    url: '/system/leave',
    method: 'put',
    data: data
  })
}

// 删除OA 请假申请
export function delLeave(id) {
  return request({
    url: '/system/leave/' + id,
    method: 'delete'
  })
}

// 提交审批OA 请假申请
export function submit(id) {
  return request({
    url: '/system/leave/submit/' + id,
    method: 'get'
  })
}

// 办理OA 请假申请
export function handle(data, taskId, skipType, message, nodeCode) {
  return request({
    url: '/system/leave/handle?taskId=' + taskId + '&skipType=' + skipType + '&message=' + message + '&nodeCode=' + nodeCode,
    data: data,
    method: 'post'
  })
}
