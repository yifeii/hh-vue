package com.ruoyi.flow.service.impl;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.flow.domain.Flow;
import com.ruoyi.flow.service.ExecuteService;
import com.warm.flow.core.dto.FlowParams;
import com.warm.flow.core.entity.Instance;
import com.warm.flow.core.service.InsService;
import com.warm.tools.utils.IdUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author minliuhua
 * @description: 流程执行serviceImpl
 * @date: 2023/5/29 13:09
 */
@Service
public class ExecuteServiceImpl implements ExecuteService {

    @Resource
    private InsService insService;


    @Override
    public void startFlow(Flow flow, String tableName) {
        Long id = IdUtils.nextId();
        LoginUser user = SecurityUtils.getLoginUser();
        FlowParams flowParams = FlowParams.build().flowCode(tableName)
                .createBy(user.getUser().getUserId().toString())
                .nickName(user.getUser().getNickName());
        Instance instance = insService.start(String.valueOf(id), flowParams);
        flow.setId(id);
        flow.setInstanceId(instance.getId());
        flow.setNodeCode(instance.getNodeCode());
        flow.setNodeName(instance.getNodeName());
        flow.setFlowStatus(instance.getFlowStatus());
    }
}
