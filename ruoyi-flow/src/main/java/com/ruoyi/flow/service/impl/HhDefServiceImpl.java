package com.ruoyi.flow.service.impl;

import com.ruoyi.flow.service.HhDefService;
import com.warm.flow.core.service.DefService;
import com.warm.flow.core.service.NodeService;
import com.warm.flow.core.service.SkipService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author minliuhua
 * @description: 流程定义serviceImpl
 * @date: 2023/5/29 13:09
 */
@Service
public class HhDefServiceImpl implements HhDefService {

    @Resource
    private DefService defService;

    @Resource
    private NodeService nodeService;

    @Resource
    private SkipService skipService;

}
