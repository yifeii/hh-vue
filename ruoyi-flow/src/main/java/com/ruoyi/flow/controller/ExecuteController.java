package com.ruoyi.flow.controller;

import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;
import com.warm.flow.core.FlowFactory;
import com.warm.flow.core.constant.FlowCons;
import com.warm.flow.core.entity.HisTask;
import com.warm.flow.core.entity.Instance;
import com.warm.flow.core.entity.Node;
import com.warm.flow.core.entity.Task;
import com.warm.flow.core.enums.NodeType;
import com.warm.flow.core.service.HisTaskService;
import com.warm.flow.core.service.InsService;
import com.warm.flow.core.service.NodeService;
import com.warm.flow.core.service.TaskService;
import com.warm.flow.orm.entity.FlowHisTask;
import com.warm.flow.orm.entity.FlowTask;
import com.warm.tools.utils.StreamUtils;
import com.warm.tools.utils.page.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 流程实例Controller
 *
 * @author hh
 * @date 2023-04-18
 */
@Validated
@RestController
@RequestMapping("/flow/execute")
public class ExecuteController extends BaseController {
    @Resource
    private ISysUserService userService;

    @Resource
    private HisTaskService hisTaskService;

    @Resource
    private TaskService taskService;

    @Resource
    private NodeService nodeService;

    @Resource
    private InsService insService;

    /**
     * 分页待办任务列表
     */
    @PreAuthorize("@ss.hasPermi('flow:execute:toDoPage')")
    @GetMapping("/toDoPage")
    public TableDataInfo toDoPage(FlowTask flowTask) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Page<Task> page = Page.pageOf(pageDomain.getPageNum(), pageDomain.getPageSize());
        List<SysRole> roles = SecurityUtils.getLoginUser().getUser().getRoles();
        List<String> roleKeyList = roles.stream().map(role ->
                "role:" + role.getRoleId().toString()).collect(Collectors.toList());
        flowTask.setPermissionList(roleKeyList);
        page = taskService.toDoPage(flowTask, page);
        page.getList().forEach(instance -> {
            if (StringUtils.isNotBlank(instance.getApprover())) {
                SysUser sysUser = userService.selectUserById(Long.valueOf(instance.getApprover()));
                if (StringUtils.isNotNull(sysUser)) {
                    instance.setApprover(sysUser.getNickName());
                }
            }
        });
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(page.getList());
        rspData.setTotal(page.getTotal());
        return rspData;
    }

    /**
     * 分页已办任务列表
     */
    @PreAuthorize("@ss.hasPermi('flow:execute:donePage')")
    @GetMapping("/donePage")
    public TableDataInfo donePage(FlowHisTask flowHisTask) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Page<HisTask> page = Page.pageOf(pageDomain.getPageNum(), pageDomain.getPageSize());
        List<SysRole> roles = SecurityUtils.getLoginUser().getUser().getRoles();
        List<String> roleKeyList = roles.stream().map(role ->
                "role:" + role.getRoleId().toString()).collect(Collectors.toList());
        flowHisTask.setPermissionList(roleKeyList);
        page = hisTaskService.donePage(flowHisTask, page);
        page.getList().forEach(hisTask -> {
            if (StringUtils.isNotBlank(hisTask.getApprover())) {
                SysUser sysUser = userService.selectUserById(Long.valueOf(hisTask.getApprover()));
                if (StringUtils.isNotNull(sysUser)) {
                    hisTask.setApprover(sysUser.getNickName());
                }
            }
        });
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(page.getList());
        rspData.setTotal(page.getTotal());
        return rspData;
    }

    /**
     * 查询已办任务历史记录
     */
    @PreAuthorize("@ss.hasPermi('flow:execute:doneList')")
    @GetMapping("/doneList/{instanceId}")
    public R<List<HisTask>> doneList(@PathVariable("instanceId") Long instanceId) {
        List<HisTask> flowHisTasks = hisTaskService.getByInsIds(instanceId);
        flowHisTasks.forEach(hisTask -> {
            if (StringUtils.isNotBlank(hisTask.getApprover())) {
                SysUser sysUser = userService.selectUserById(Long.valueOf(hisTask.getApprover()));
                if (StringUtils.isNotNull(sysUser)) {
                    hisTask.setApprover(sysUser.getNickName());
                }
                hisTask.setApprover(sysUser.getNickName());
            }
        });
        return R.ok(flowHisTasks);
    }

    /**
     * 根据节点code查询代表任务
     * @param nodeCode
     * @return
     */
    @GetMapping("/flowTaskById/{nodeCode}/{instanceId}")
    public R<Task> flowTaskById(@PathVariable("nodeCode") String nodeCode, @PathVariable("instanceId") Long instanceId) {
        return R.ok(taskService.getOne(new FlowTask().setNodeCode(nodeCode).setInstanceId(instanceId)));
    }

    /**
     * 查询跳转任意节点列表
     */
    @PreAuthorize("@ss.hasPermi('flow:execute:doneList')")
    @GetMapping("/anyNodeList/{instanceId}/{nodeCode}")
    public R<List<Node>> anyNodeList(@PathVariable("instanceId") Long instanceId
            , @PathVariable("nodeCode") String nodeCode) {
        Instance instance = insService.getById(instanceId);
        Node node = nodeService.getOne(FlowFactory.newNode().setDefinitionId(instance.getDefinitionId()).setNodeCode(nodeCode));
        if (FlowCons.SKIP_ANY_N.equals(node.getSkipAnyNode())) {
            return R.ok();
        }
        List<Node> nodeList = nodeService.list(FlowFactory.newNode().setDefinitionId(instance.getDefinitionId()));
        nodeList = StreamUtils.filter(nodeList, n -> (NodeType.isBetween(n.getNodeType())
                || NodeType.isEnd(n.getNodeType()))&& !nodeCode.equals(n.getNodeCode()));
        return R.ok(nodeList);
    }
}
