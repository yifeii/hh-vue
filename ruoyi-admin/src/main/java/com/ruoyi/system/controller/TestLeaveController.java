package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TestLeave;
import com.ruoyi.system.service.ITestLeaveService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * OA 请假申请Controller
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/system/leave")
public class TestLeaveController extends BaseController
{
    @Autowired
    private ITestLeaveService testLeaveService;

    /**
     * 查询OA 请假申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:leave:list')")
    @GetMapping("/list")
    public TableDataInfo list(TestLeave testLeave)
    {
        startPage();
        List<TestLeave> list = testLeaveService.selectTestLeaveList(testLeave);
        return getDataTable(list);
    }

    /**
     * 导出OA 请假申请列表
     */
    @PreAuthorize("@ss.hasPermi('system:leave:export')")
    @Log(title = "OA 请假申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TestLeave testLeave)
    {
        List<TestLeave> list = testLeaveService.selectTestLeaveList(testLeave);
        ExcelUtil<TestLeave> util = new ExcelUtil<TestLeave>(TestLeave.class);
        util.exportExcel(response, list, "OA 请假申请数据");
    }

    /**
     * 获取OA 请假申请详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:leave:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(testLeaveService.selectTestLeaveById(id));
    }

    /**
     * 新增OA 请假申请
     */
    @PreAuthorize("@ss.hasPermi('system:leave:add')")
    @Log(title = "OA 请假申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TestLeave testLeave)
    {
        return toAjax(testLeaveService.insertTestLeave(testLeave));
    }

    /**
     * 修改OA 请假申请
     */
    @PreAuthorize("@ss.hasPermi('system:leave:edit')")
    @Log(title = "OA 请假申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TestLeave testLeave)
    {
        return toAjax(testLeaveService.updateTestLeave(testLeave));
    }

    /**
     * 删除OA 请假申请
     */
    @PreAuthorize("@ss.hasPermi('system:leave:remove')")
    @Log(title = "OA 请假申请", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(testLeaveService.deleteTestLeaveByIds(ids));
    }

    /**
     * 提交审批
     */
    @PreAuthorize("@ss.hasPermi('system:leave:submit')")
    @Log(title = "OA 请假申请", businessType = BusinessType.OTHER)
    @GetMapping(value = "/submit/{id}")
    public AjaxResult submit(@PathVariable("id") String id) {
        return toAjax(testLeaveService.submit(id));
    }

    /**
     * 办理
     */
    @PreAuthorize("@ss.hasPermi('system:execute:handle')")
    @Log(title = "流程实例", businessType = BusinessType.OTHER)
    @PostMapping("/handle")
    public AjaxResult handle(@RequestBody TestLeave testLeave, Long taskId, String skipType, String message, String nodeCode) {
        return toAjax(testLeaveService.handle(testLeave, taskId, skipType, message, nodeCode));
    }
}
