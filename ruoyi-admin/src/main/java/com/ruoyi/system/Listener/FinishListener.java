package com.ruoyi.system.Listener;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.TestLeave;
import com.ruoyi.system.mapper.TestLeaveMapper;
import com.warm.flow.core.entity.Instance;
import com.warm.flow.core.listener.Listener;
import com.warm.flow.core.listener.ListenerVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
public class FinishListener implements Listener {

    @Resource
    private TestLeaveMapper testLeaveMapper;

    private static final Logger log = LoggerFactory.getLogger(StartListener.class);

    @Override
    public void notify(ListenerVariable variable) {
        log.info("完成监听器:{}", variable);
        Instance instance = variable.getInstance();
        Map<String, Object> testLeaveMap = variable.getVariable();
        TestLeave testLeave = (TestLeave) testLeaveMap.get("testLeave");
        /** 如果{@link com.ruoyi.system.service.impl.TestLeaveServiceImpl}中更新了，这里就不用更新了*/
//        testLeave.setNodeCode(instance.getNodeCode());
//        testLeave.setNodeName(instance.getNodeName());
//        testLeave.setFlowStatus(instance.getFlowStatus());
//        testLeave.setUpdateTime(DateUtils.getNowDate());
//        testLeaveMapper.updateTestLeave(testLeave);
        log.info("完成监听器结束;{}", "任务完成");
    }
}
